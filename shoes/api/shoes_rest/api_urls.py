from django.urls import path

from .api_views import (
    api_list_shoes,
    api_list_bins,
    api_show_shoes,

)


urlpatterns = [
    path('shoes/', api_list_shoes, name = 'api_list_shoes'),
    path('bins/', api_list_bins, name = 'binvo_list'),
    path('shoes/<int:pk>/', api_show_shoes, name = 'shoes_detail'),
    path('shoes/<int:pk>/', api_show_shoes, name = 'delete_shoes'),

]
