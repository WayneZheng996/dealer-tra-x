from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from .models import Shoe, BinVO
from django.http import JsonResponse
import json
from common.json import ModelEncoder


class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = ['href', 'bin_number', 'closet_name', 'bin_size',]


class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        'id',
        'manufacturer',
        'model_name',
        'color',
        'picture_url',
        'bin',

    ]

    encoders = {
        "bin": BinVOEncoder()
    }


@require_http_methods(['GET', 'POST'])
def api_list_shoes(request):

    if request.method == "GET":
        shoes = Shoe.objects.all()

        return JsonResponse(
            {'shoes': shoes},
            encoder = ShoeListEncoder,
        )

    else:
        content = json.loads(request.body)

        try:
            if type(content['bin']) != int:
                bin_href = content['bin']


            else:
                bin_href = f'/api/bins/{content["bin"]}/'
            bin = BinVO.objects.get(href=bin_href)
            content['bin'] = bin


        except BinVO.DoesNotExist:
            return JsonResponse(
                {"Message": "Bad bin id"},
                    status = 400,)


        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeListEncoder,
            safe=False,
        )



@require_http_methods(["DELETE", "GET"])
def api_show_shoes(request, pk):

    if request.method == "GET":

        try:
            shoe = Shoe.objects.get(id=pk)
            return JsonResponse(shoe, encoder = ShoeListEncoder, safe=False)

        except:
            return JsonResponse(
                {"Message": "Bad shoe id"},
                 status=400,)


    else:
        count, _ = Shoe.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})


@require_http_methods(["GET"])
def api_list_bins(request):

    if request.method == 'GET':
        bins = BinVO.objects.all()
        return JsonResponse(
            {'bins': bins},
             encoder=BinVOEncoder)
