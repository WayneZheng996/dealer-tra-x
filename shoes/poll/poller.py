import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "shoes_project.settings")
django.setup()

from shoes_rest.models import BinVO
# Import models from hats_rest, here.
# from shoes_rest.models import Something



def get_bins():
    response = requests.get('http://wardrobe-api:8000/api/bins/')
    content = json.loads(response.content)
    for i in content['bins']: #bins from foreign key on Shoe model
        BinVO.objects.update_or_create(
            href = i['href'],    # href from BinVO model
            defaults = {
                "closet_name": i["closet_name"],
                "bin_number": i["bin_number"],
                "bin_size": i["bin_size"],


            },

        )

def poll():
    while True:
        print('Shoes poller polling for data')
        try:
            # Write your polling logic, here
            get_bins()
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    poll()
