from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import Hat, LocationVO


class LocationVoEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        'import_href',
        'closet_name',
        'section_number',
        'shelf_number',
    ]

class HatsEncoder(ModelEncoder):
    model = Hat
    properties = [
        'fabric',
        'style_name',
        'color',
        'picture_url',
        'location'
    ]
    encoders = {
        "location": LocationVoEncoder(),
    }



@require_http_methods(['GET','POST'])
def get_hats_list(request):
    if request.method == 'POST':
        content = json.loads(request.body)
        try:
            if type(content['location']) != int:
                href_link = content['location']
            else:
                href_link = f'/api/locations/{content["location"]}/'
            location = LocationVO.objects.get(import_href=href_link)
            content['location'] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"Message": 'Invalid Location ID or Link'},
                status=400
            )
        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatsEncoder,
            safe=False
        )
    else:
        hat = Hat.objects.all()
        return JsonResponse(
            {"hat":hat}, encoder=HatsEncoder
        )


@require_http_methods(['GET','DELETE', 'PUT'])
def get_hats_detail(request, pk):
    if request.method == "DELETE":
        count, _ = Hat.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        hat = Hat.objects.get(id=pk)
        return JsonResponse(
            hat,
            encoder=HatsEncoder,
            safe=False
        )
