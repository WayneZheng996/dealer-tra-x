from django.db import models
from django.urls import reverse

# Create your models here.

class LocationVO(models.Model):
    closet_name = models.CharField(max_length=100)
    section_number = models.PositiveSmallIntegerField()
    shelf_number = models.PositiveSmallIntegerField()
    import_href = models.CharField(max_length=200, unique=True, null=True)

    def __str__(self) -> str:
        return self.closet_name


class Hat(models.Model):
    fabric = models.CharField(max_length=200)
    style_name = models.CharField(max_length=200)
    color = models.CharField(max_length=200)
    picture_url = models.URLField(null=True)
    location = models.ForeignKey(
        LocationVO,
        related_name='location',
        on_delete=models.CASCADE,
        null=True)

    def __str__(self) -> str:
        return self.style_name

    def get_api_url(self):
        return reverse("get_hats_detail", kwargs={"pk": self.pk})
