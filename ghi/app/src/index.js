import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

async function loadHats() {
  const response = await fetch('http://localhost:8090/api/hats');
  if (!response.ok){
    return console.error(response)
  }
  let jsonData = await response.json()
  root.render(
    <React.StrictMode>
      <App hats={jsonData.hat} />
    </React.StrictMode>
  );
}
loadHats();
