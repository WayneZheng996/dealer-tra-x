import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatsList from './hats';
import HatsForm from './hats-form';
import ShoesList from './shoes';
import ShoesForm from './shoes-form';


function App(props) {
  if (props.hats === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/hats" element={<HatsList hats={props.hats} />} />
          <Route path="/hats/create" element={<HatsForm/>} />
          <Route path="/shoes/create" element={<ShoesForm/>} />
          <Route path="/shoes" element={<ShoesList hats={props.hats} />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
