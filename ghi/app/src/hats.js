import {useNavigate} from 'react-router-dom';

function HatsList(props){
    const navigate = useNavigate();

    const navigateToCreatePage = () =>{
        navigate('/hats/create')
    }

    return (
        <div>
            <table className="table table-striped">
            <thead>
            <tr>
                <th>Fabric</th>
                <th>Style Name</th>
                <th>Color</th>
                <th>Location-Section-Shelf</th>
                <th>Image</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            {/* for (let attendee of props.attendees) {
                <tr>
                <td>{ attendee.name }</td>
                <td>{ attendee.conference }</td>
                </tr>
            } */}
            {props.hats.map(hat => {
                return (
                <tr key={hat.href}>
                    <td>{ hat.fabric }</td>
                    <td>{ hat.style_name }</td>
                    <td>{ hat.color }</td>
                    <td>{ `${hat.location.closet_name} - ${hat.location.section_number} - ${hat.location.shelf_number}` }</td>
                    <td><img src={hat.picture_url} alt={hat.href} width="150" height="150" /></td>
                    <td><button type="button" className="btn btn-primary" onClick={
                        async () => {
                            const response = await fetch(`http://localhost:8090${hat.href}`, {method:"DELETE"})
                            const data = await response.json()
                            console.log(data)
                            window.location.reload()
                        }
                        }>Delete</button></td>
                </tr>
                );
            })}
            </tbody>
        </table>
        <div className="col text-center">
            <button type="button" className="btn btn-primary" onClick={navigateToCreatePage} >Add a New Hat</button>
        </div>
      </div>

    );
}


export default HatsList;
